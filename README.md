# Exeet

An elixir implementation of the [Twitter Kata by Sandro
Mancuso](https://github.com/sandromancuso/twitter-kata-java).

## Installation

Make sure you have both Elixir and Erlang available. Then run `mix deps.get` to
fetch all the required dependencies.

## Running the tests

``` shellsession
$ mix test --trace
```

