defmodule Exeet.Core.MessagesTest do
  use ExUnit.Case
  alias Exeet.Core.Messages
  alias Exeet.Types.Message

  test "A message can be post" do
    data = %Message{id: "1", username: "alice", message: "hello world"}
    [message | _] = Messages.post([], data)
    assert message.id == "1"
    assert message.username == "alice"
    assert message.message == "hello world"
    assert is_nil(message.created_at) == false
  end

  test "Wall for users can be retrieved" do
    messages = [
      %Message{
        id: "1",
        username: "alice",
        message: "msg 1",
        created_at: ~U[2020-11-13 17:28:00.0Z]
      },
      %Message{
        id: "2",
        username: "bob",
        message: "msg 2",
        created_at: ~U[2020-11-13 17:28:01.0Z]
      },
      %Message{
        id: "3",
        username: "alice",
        message: "msg 3",
        created_at: ~U[2020-11-13 17:28:02.0Z]
      },
      %Message{
        id: "4",
        username: "alice",
        message: "msg 4",
        created_at: ~U[2020-11-13 17:28:03.0Z]
      },
      %Message{
        id: "5",
        username: "charlie",
        message: "msg 1",
        created_at: ~U[2020-11-13 17:28:04.0Z]
      }
    ]

    wall = Messages.wall(messages, ["alice"])
    assert(length(wall) == 3)
    assert(wall == new_first(wall))

    wall = Messages.wall(messages, ["bob", "charlie", "notfound"])
    assert(length(wall) == 2)
    assert(wall == new_first(wall))
  end

  defp new_first(messages) do
    messages
    |> Enum.sort(fn a, b -> DateTime.compare(a.created_at, b.created_at) != :lt end)
  end
end
