defmodule Exeet.Core.Messages do
  @moduledoc """
  Module for handling exeeted messages.
  """

  alias Exeet.Types.Message

  def post(messages, %Message{} = message) do
    messages ++ [%{message | created_at: DateTime.now!("Etc/UTC")}]
  end

  def wall(messages, usernames) do
    messages
    |> by_username(usernames)
    |> new_first()
  end

  defp by_username(messages, usernames) do
    messages
    |> Enum.filter(fn message ->
      Enum.any?(usernames, fn username -> username == message.username end)
    end)
  end

  defp new_first(messages) do
    messages
    |> Enum.sort(fn a, b -> DateTime.compare(a.created_at, b.created_at) != :lt end)
  end
end
