defmodule Exeet.Types.Message do
  @moduledoc """
  The Message type
  """
  use TypedStruct

  typedstruct do
    field(:id, String.t())
    field(:username, String.t())
    field(:message, String.t())
    field(:created_at, DateTime)
  end
end
